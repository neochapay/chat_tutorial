#include <QtQuick>
#include <sailfishapp.h>
#include <QtDBus>
#include "ChatConnection.h"
#include "WindowAdaptor.h"

static const QString s_dbusService = QStringLiteral("ru.omprussia.chatclient");
static const QString s_dbusObject = QStringLiteral("/ru/omprussia/chatclient");
static const QString s_dbusInterface = QStringLiteral("ru.omprussia.chatclient");
static const QString s_dbusActivateMethod = QStringLiteral("Activate");

int main(int argc, char *argv[])
{
    if (QDBusConnection::sessionBus().interface()->isServiceRegistered(s_dbusService)) {
        qDebug() << "Activating application via D-Bus";
        QDBusMessage activateMessage = QDBusMessage::createMethodCall(s_dbusService, s_dbusObject, s_dbusInterface, s_dbusActivateMethod);
        QDBusReply<void> reply = QDBusConnection::sessionBus().call(activateMessage);
        if (!reply.isValid()) {
            qWarning() << reply.error().message();
        }
        return 0;
    }

    qmlRegisterType<ChatConnection>("Chat", 1, 0, "ChatConnection");
    QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));
    app->setQuitOnLastWindowClosed(false);
    QScopedPointer<QQuickView> view(SailfishApp::createView());
    view->setSource(SailfishApp::pathToMainQml());
    view->show();

    new WindowAdaptor(view.data());
    QTimer::singleShot(0, [&view]() {
        QDBusConnection session = QDBusConnection::sessionBus();
        const bool objectRegistered = session.registerObject(s_dbusObject, view.data());
        if (!objectRegistered) {
            qWarning() << "Cannot register object:" << session.lastError().message();
            qApp->quit();
            return;
        }
        qDebug() << "Registered D-Bus object:" << s_dbusObject;
        const bool serviceRegistereg = session.registerService(s_dbusService);
        if (!serviceRegistereg) {
            qWarning() << "Cannot register service:" << session.lastError().message();
            qApp->quit();
            return;
        }
        qDebug() << "Registered D-Bus service:" << s_dbusService;
    });

    return app->exec();
}
