#ifndef CHATCONNECTION_H
#define CHATCONNECTION_H

#include <QWebSocket>

class ChatConnection : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUrl url READ url NOTIFY urlChanged)

public:
    enum MessageType {
        Handshake         = 1,
        HandshakeResponse = 2,
        Text              = 3,
        File              = 4
    };

    explicit ChatConnection(QWebSocket *webSocket = nullptr, QObject *parent = nullptr);
    QUrl url() const;
    bool isConnected() const;

signals:
    void connected();
    void disconnected();
    void urlChanged();
    void error(const QString &errorMessage);

    void handshakeReceived(const QString &userName);
    void handshakeResponseReceived(const QString &userName, bool result);

    void textReceived(const QString &senderName, const QString &text);
    void fileReceived(const QString &senderName, const QString &fileName, const QString &cachePath);

public slots:
    void connect(const QUrl &url);
    void disconnect();

    void sendHandshake(const QString &userName);
    void sendHandshakeResponse(const QString &userName, bool result);

    void sendText(const QString &senderName, const QString &text);
    void sendFile(const QString &senderName, const QString &fileName, const QString &path);

private:
    QWebSocket *_socket;

private slots:
    void _sendTextMessage(const QJsonObject &meta);
    void _onTextMessageReceived(const QString &message);
    void _sendBinaryMessage(const QJsonObject &meta, const QByteArray &data = QByteArray());
    void _onBinaryMessageReceived(const QByteArray &message);
    void _routeJsonReceived(const QJsonObject &meta, const QByteArray &data = QByteArray());
    void _processHandshakeReceived(const QJsonObject &meta);
    void _processHandshakeResponseReceived(const QJsonObject &meta);
    void _processTextReceived(const QJsonObject &meta);
    void _processFileReceived(const QJsonObject &meta, const QByteArray &data);
};

#endif // CHATCONNECTION_H
