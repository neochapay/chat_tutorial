#ifndef WINDOWADAPTOR_H
#define WINDOWADAPTOR_H

#include <QDBusAbstractAdaptor>

class QQuickView;
class WindowAdaptor : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "ru.omprussia.chatclient")
public:
    explicit WindowAdaptor(QObject *parent);

public slots:
    void Activate();

private:
    QQuickView *_view = nullptr;
};

#endif // WINDOWADAPTOR_H
