#include "ChatConnection.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QFileInfo>
#include <QStandardPaths>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QDataStream>

ChatConnection::ChatConnection(QWebSocket *webSocket, QObject *parent) :
    QObject(parent)
{
    this->_socket = webSocket ? webSocket : new QWebSocket();
    this->_socket->setParent(this);
    QObject::connect(this->_socket, &QWebSocket::connected,
                     this,          &ChatConnection::connected);
    QObject::connect(this->_socket, &QWebSocket::disconnected,
                     this,          &ChatConnection::disconnected);
    QObject::connect(this->_socket, static_cast<void (QWebSocket::*)(QAbstractSocket::SocketError)>(&QWebSocket::error),
                     [=] () { emit this->error(this->_socket->errorString()); });
    QObject::connect(this->_socket, &QWebSocket::textMessageReceived,
                     this,          &ChatConnection::_onTextMessageReceived);
    QObject::connect(this->_socket, &QWebSocket::binaryMessageReceived,
                     this,          &ChatConnection::_onBinaryMessageReceived);
}

QUrl ChatConnection::url() const
{
    return this->_socket->requestUrl();
}

bool ChatConnection::isConnected() const
{
    return this->_socket->state() == QAbstractSocket::ConnectedState;
}

void ChatConnection::connect(const QUrl &url)
{
    this->_socket->open(url);
    emit this->urlChanged();
}

void ChatConnection::disconnect()
{
    if (this->_socket->state() != QAbstractSocket::UnconnectedState)
        this->_socket->close();
}

void ChatConnection::sendHandshake(const QString &userName)
{
    QJsonObject meta;
    meta["messageType"] = Handshake;
    meta["userName"] = userName;
    this->_sendBinaryMessage(meta);
}

void ChatConnection::sendHandshakeResponse(const QString &userName, bool result)
{
    QJsonObject meta;
    meta["messageType"] = HandshakeResponse;
    meta["userName"] = userName;
    meta["result"] = result;
    this->_sendBinaryMessage(meta);
}

void ChatConnection::sendText(const QString &senderName, const QString &text)
{
    QJsonObject meta;
    meta["messageType"] = Text;
    meta["senderName"] = senderName;
    meta["text"] = text;
    this->_sendBinaryMessage(meta);
}

void ChatConnection::sendFile(const QString &senderName, const QString &fileName, const QString &path)
{
    QJsonObject meta;
    meta["messageType"] = File;
    meta["senderName"] = senderName;
    meta["fileName"] = fileName;
    QFile file(path);
    file.open(QIODevice::ReadOnly);
    QByteArray data = file.readAll();
    file.close();
    this->_sendBinaryMessage(meta, data);
}

void ChatConnection::_sendTextMessage(const QJsonObject &meta)
{
    if (!this->isConnected())
        return;
    QJsonDocument metaDocument(meta);
    QString message = metaDocument.toJson(QJsonDocument::Compact);
    this->_socket->sendTextMessage(message);
}

void ChatConnection::_onTextMessageReceived(const QString &message)
{
    QJsonParseError parseError;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(message.toUtf8(), &parseError);
    if (parseError.error == QJsonParseError::NoError) {
        QJsonObject jsonObject = jsonDocument.object();
        this->_routeJsonReceived(jsonObject);
    } else {
        emit this->error(QObject::tr("Cannot parse incoming text message: %1").arg(message));
    }
}

void ChatConnection::_sendBinaryMessage(const QJsonObject &meta, const QByteArray &data)
{
    if (!this->isConnected())
        return;
    QJsonDocument metaDocument(meta);
    QByteArray message = metaDocument.toBinaryData().append(data);
    this->_socket->sendBinaryMessage(message);
}

void ChatConnection::_onBinaryMessageReceived(const QByteArray &message)
{
    QJsonDocument metaDocument = QJsonDocument::fromBinaryData(message);
    int dataOffset = 0;
    metaDocument.rawData(&dataOffset);
    QJsonObject meta = metaDocument.object();
    QByteArray data = message.mid(dataOffset);
    this->_routeJsonReceived(meta, data);
}

void ChatConnection::_routeJsonReceived(const QJsonObject &meta, const QByteArray &data)
{
    switch (meta.value("messageType").toInt()) {
    case Handshake:
        return this->_processHandshakeReceived(meta);
    case HandshakeResponse:
        return this->_processHandshakeResponseReceived(meta);
    case Text:
        return this->_processTextReceived(meta);
    case File:
        return this->_processFileReceived(meta, data);
    default:
        emit this->error(QObject::tr("Unknown messageType: %1").arg(meta.value("messageType").toString()));
    }
}

void ChatConnection::_processHandshakeReceived(const QJsonObject &meta)
{
    QString userName = meta.value("userName").toString();
    emit this->handshakeReceived(userName);
}

void ChatConnection::_processHandshakeResponseReceived(const QJsonObject &meta)
{
    QString userName = meta.value("userName").toString();
    bool result = meta.value("result").toBool();
    emit this->handshakeResponseReceived(userName, result);
}

void ChatConnection::_processTextReceived(const QJsonObject &meta)
{
    QString senderName = meta.value("senderName").toString();
    QString text = meta.value("text").toString();
    emit this->textReceived(senderName, text);
}

void ChatConnection::_processFileReceived(const QJsonObject &meta, const QByteArray &data)
{
    QString senderName = meta.value("senderName").toString();
    QString fileName = meta.value("fileName").toString();
    QDir cacheDir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
    cacheDir.mkpath(".");
    QString cacheFileName = QDateTime::currentDateTime().toString("yyyy-MM-dd_HH:mm:ss.zzz_%1").arg(fileName);
    QFileInfo cacheFileInfo(cacheDir, cacheFileName);
    QFile cacheFile(cacheFileInfo.filePath());
    cacheFile.open(QIODevice::WriteOnly);
    cacheFile.write(data);
    cacheFile.close();
    emit this->fileReceived(senderName, fileName, cacheFileInfo.filePath());
}
