#include "WindowAdaptor.h"

#include <QQuickView>
#include <QDebug>

WindowAdaptor::WindowAdaptor(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
    _view = qobject_cast<QQuickView*>(parent);
}

void WindowAdaptor::Activate()
{
    if (!_view) {
        qWarning() << Q_FUNC_INFO << "view is not defined!";
        return;
    }

    qDebug() << Q_FUNC_INFO;

    _view->show();
    _view->raise();
}
