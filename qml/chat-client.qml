import Sailfish.Silica 1.0

ApplicationWindow {
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    initialPage: Qt.resolvedUrl("pages/MainPage.qml")
    allowedOrientations: defaultAllowedOrientations
}
