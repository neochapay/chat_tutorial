import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    Label {
        text: qsTr("Chat Client")
        anchors { fill: parent; margins: Theme.horizontalPageMargin }
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.WordWrap
    }
}
