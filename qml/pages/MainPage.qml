import QtQuick 2.0
import Sailfish.Silica 1.0
import "../assets"
import Chat 1.0

Page {
    property url serverUrl: "ws://changeme"
    property string userName: "Username"
    property bool loggedIn: false

    ChatConnection {
        id: chatConnection

        onConnected: {
            chatView.postInfo(qsTr("Connected"));
            sendHandshake(userName);
            chatView.scrollToBottom();
        }
        onDisconnected: {
            loggedIn = false;
            chatView.postInfo(qsTr("Disconnected"));
            chatView.scrollToBottom();
        }
        onHandshakeResponseReceived: {
            if (result) {
                chatView.postInfo(qsTr("Authorised as %1").arg(userName));
            } else {
                chatView.postInfo(qsTr("Cannot authorised as %1").arg(userName));
            }
            loggedIn = result;
        }
        onTextReceived: {
            chatView.postIncomingText(text, senderName);
            chatView.scrollToBottom();
        }
        onFileReceived: {
            console.log(cachePath);
            chatView.postIncomingImage(cachePath, senderName);
            chatView.scrollToBottom();
        }
        Component.onCompleted: connect(serverUrl)
    }
    SilicaFlickable {
        anchors.fill: parent

        Column {
            width: parent.width
            height: parent.height

            ChatView {
                id: chatView

                width: parent.width
                height: parent.height - inputArea.height
                clip: true
                header: PageHeader { title: qsTr("Chat Client") }
            }
            Rectangle {
                id: inputArea

                width: parent.width
                height: childrenRect.height
                color: Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)

                TextField {
                    width: parent.width
                    enabled: loggedIn
                    placeholderText: qsTr("Input message")
                    label: qsTr("Message")
                    EnterKey.enabled: text.length > 0
                    EnterKey.iconSource: "image://theme/icon-m-sms"
                    EnterKey.onClicked: {
                        chatConnection.sendText(userName, text);
                        chatView.postOutgoingText(text, qsTr("Me"));
                        chatView.scrollToBottom();
                        text = "";
                    }
                }
            }
        }
        PushUpMenu {
            quickSelect: true

            MenuItem {
                text: qsTr("Quit")
                onClicked: Qt.quit()
            }

            MenuItem {
                text: qsTr("Send image")

                onClicked: {
                    var imagePicker = pageStack.push("Sailfish.Pickers.ImagePickerPage");
                    imagePicker.selectedContentChanged.connect(function () {
                        var fileName = imagePicker.selectedContentProperties.fileName;
                        var filePath = imagePicker.selectedContentProperties.filePath;
                        chatConnection.sendFile(userName, fileName, filePath);
                        chatView.postOutgoingImage(filePath, qsTr("Me"));
                        chatView.scrollToBottom();
                    });
                }
            }
        }
    }
}
