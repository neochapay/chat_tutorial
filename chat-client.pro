TARGET = chat-client

QT += websockets dbus

CONFIG += sailfishapp

HEADERS += \
    src/ChatConnection.h \
    src/WindowAdaptor.h

SOURCES += \
    src/chat-client.cpp \
    src/ChatConnection.cpp \
    src/WindowAdaptor.cpp

DISTFILES += \
    qml/chat-client.qml \
    qml/assets/ChatView.qml \
    qml/cover/CoverPage.qml \
    qml/pages/MainPage.qml \
    rpm/chat-client.yaml \
    rpm/chat-client.spec \
    translations/*.ts \
    chat-client.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += sailfishapp_i18n
TRANSLATIONS += translations/chat-client-ru.ts
