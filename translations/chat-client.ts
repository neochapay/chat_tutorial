<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>ChatView</name>
    <message>
        <location filename="../qml/assets/ChatView.qml" line="60"/>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../qml/assets/ChatView.qml" line="132"/>
        <source>&lt;b&gt;%1&lt;/b&gt;, %2</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;, %2</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="6"/>
        <source>Chat Client</source>
        <translation>Chat Client</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="28"/>
        <source>Cannot authorised as %1</source>
        <translation>Cannot authorised as %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="26"/>
        <source>Authorised as %1</source>
        <translation>Authorised as %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="15"/>
        <source>Connected</source>
        <translation>Connected</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="21"/>
        <source>Disconnected</source>
        <translation>Disconnected</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="56"/>
        <source>Chat Client</source>
        <translation>Chat Client</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="68"/>
        <source>Input message</source>
        <translation>Input message</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="69"/>
        <source>Message</source>
        <translation>Message text</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="74"/>
        <location filename="../qml/pages/MainPage.qml" line="93"/>
        <source>Me</source>
        <translation>Me</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="85"/>
        <source>Send image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/ChatConnection.cpp" line="106"/>
        <source>Cannot parse incoming text message: %1</source>
        <translation>Cannot parse incoming text message: %1</translation>
    </message>
    <message>
        <location filename="../src/ChatConnection.cpp" line="141"/>
        <source>Unknown messageType: %1</source>
        <translation>Unknown messageType: %1</translation>
    </message>
</context>
</TS>
